<!doctype html>
<html lang="en">
 <head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="UTF-8">
  <meta name="Generator" content="EditPlus®">
  <meta name="Author" content="">
  <meta name="Keywords" content="">
  <meta name="Description" content="">
  <script src="jquery-2.0.3.min.js"></script>
  <?php
//	$dir = getcwd();
	function dir_init($dir_temp = "Action_Comics_Annual_001"){
		global $dir,$a;
		$dir = $dir_temp;
		// Sort in ascending order - this is default
		$a = scandir($dir);
		array_shift($a);
		array_shift($a);// ".", ".."을 없애고
		echo $dir."<br/>";
	}
//	echo "post값:".$_POST['dir']."<br/>";
	if($_POST['dir']) dir_init($_POST['dir']);
	else dir_init();
  ?>

  <script>
	var dir_name = "<?echo $dir;?>";
	var img_list = <?echo json_encode($a);?>;
	var curr_page = 1;
	var max_page = <?echo count($a);?>;
	var img_width, img_height;
	var img_ratio ; //이미지 비율. 2장짜리 한장으로 나눌 때 사용

	$().ready(function(){
		$("#curr_img").attr("src","./"+dir_name+"/"+img_list[curr_page-1]);
		$("title,#info").text(img_list[curr_page-1] + " ["+curr_page+"/"+max_page+"]");
	});

	function move_page(IsNext){ //페이지 이동에 쓰이는 함수
		if(IsNext){	
			if(++curr_page > max_page) curr_page=1;}
		else{
			if(--curr_page < 1) curr_page=max_page;}
		$("#curr_img").attr("src","./"+dir_name+"/"+img_list[curr_page-1]);
		$("title,#info").text(img_list[curr_page-1] + " ["+curr_page+"/"+max_page+"]");
		window.scrollTo(0,0);
	}
	
  </script>
  <title>이미지뷰어</title>
 </head>
 <body>
	<?php
	  $d = opendir(getcwd());
	  $output = "<form action=".$_SERVER['PHP_SELF']." method='post'><select name='dir'>";
	  while (($file = readdir($d)) !== false){
        if($file != "." && $file != ".." && is_dir($file)) 
			$output .=  "<option value='".$file."'>".$file."</option>";
		//echo "filename:" . $file . "<br>";
      }
	  $output .= "</select><input type='submit' value='변경'></form>";
	  echo $output;
    closedir($d);
	?>
   <img id="curr_img" src='' style='width:100%'>
   <div>
    <div id="info">page_info_here</div>
    <button onclick="move_page(false)">←</button>
    <button onclick="move_page(true)">→</button>
   </div>
 </body>
</html>
